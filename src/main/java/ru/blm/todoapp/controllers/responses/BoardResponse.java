package ru.blm.todoapp.controllers.responses;

import lombok.Data;

@Data
public class BoardResponse {
    private Long boardID;
    private String title;
    private Long userID;
}
