package ru.blm.todoapp.controllers.responses;

import lombok.Data;

import java.util.Date;

@Data
public class CardResponse {
    private Long id;
    private String title;
    private String description;
    private String status;
    private boolean complete;
    private Date endDate;
    private Long userID;
    private Long boardID;
}
