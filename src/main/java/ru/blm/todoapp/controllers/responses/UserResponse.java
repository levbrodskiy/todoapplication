package ru.blm.todoapp.controllers.responses;

import lombok.Data;

@Data
public class UserResponse {
    private Long id;
    private String email;
}
