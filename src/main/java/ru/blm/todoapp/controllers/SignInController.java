package ru.blm.todoapp.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import ru.blm.todoapp.database.entities.User;
import ru.blm.todoapp.services.exceptions.AuthorizationException;
import ru.blm.todoapp.services.implementations.UserServiceImpl;

@Controller
@RequestMapping("/sign-in")
public class SignInController {
    @Autowired
    private UserServiceImpl userService;

    @GetMapping
    public String getSignInPage() {
        return "sign-in";
    }

    @PostMapping
    public String signIn(@RequestParam(name = "email") String email,
                         @RequestParam(name = "password") String password,
                         Model model) {
        try {
            User u = userService.authorize(email, password);
            return "redirect:/user" + u.getId() + "/boards";

        } catch (AuthorizationException e) {
            return "error";
        }
    }
}
