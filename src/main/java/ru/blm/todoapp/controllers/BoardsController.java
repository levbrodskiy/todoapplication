package ru.blm.todoapp.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import ru.blm.todoapp.controllers.responses.BoardResponse;
import ru.blm.todoapp.controllers.responses.UserResponse;
import ru.blm.todoapp.database.entities.Board;
import ru.blm.todoapp.database.exceptions.LimitOfBoardsException;
import ru.blm.todoapp.database.exceptions.UserNotFoundException;
import ru.blm.todoapp.services.implementations.BoardsService;
import ru.blm.todoapp.services.implementations.UserServiceImpl;
import java.util.ArrayList;
import java.util.List;

@Controller
public class BoardsController {

    @Autowired
    private BoardsService boardsService;
    @Autowired
    private UserServiceImpl userService;

    @GetMapping("/user{user_id}/boards")
    public String getBoards(@PathVariable(name = "user_id")Long userID,
                            Model model) {
        try {
            List<BoardResponse> responses = userService.getBoardResponsesByUserID(userID);
            UserResponse response = userService.getUserResponse(userID);
            model.addAttribute("boards", responses);
            model.addAttribute("user", response);
            return "boards";
        } catch (UserNotFoundException e) {
            return "error";
        }
    }

    @GetMapping("/user{user_id}/boards/add-board")
    public String getAddBoardPage() {
        return "addBoard";
    }

    @PostMapping("/user{user_id}/boards/add-board")
    public String addBoard(@PathVariable(name = "user_id")Long userID,
                           @RequestParam(name = "title") String boardName,
                           Model model) {
        try {
            boardsService.addBoardToUser(new Board(boardName), userID);
        } catch (UserNotFoundException e) {
            return "error";
        } catch (LimitOfBoardsException e) {
            return "error";
        } catch (Exception e) {
            e.printStackTrace();
        }

        return "redirect:/user"+userID+"/boards";
    }

    @GetMapping("/user{user_id}/boards/board{board_id}/delete")
    public String deleteBoard(@PathVariable(name = "user_id")Long userID,
                              @PathVariable(name = "board_id") Long boardID,
                              Model model) {
        try {
            boardsService.deleteBoard(boardID);
        } catch (Exception e) {
           model.addAttribute("title", "Неверные данные");
           return "error";
        }

        return "redirect:/user" + userID + "/boards";
    }

    @GetMapping("/user{user_id}/boards/board{board_id}/edit")
    public String getUpdateBoardPage(@PathVariable(name = "user_id")Long userID,
                                    @PathVariable(name = "board_id") Long boardID,
                                    Model model) {
        try {
            BoardResponse response = boardsService.getBoardResponse(boardID);

            model.addAttribute("board", response);

            return "editBoard";
        } catch (Exception e) {
            model.addAttribute("title", "Неверные данные");
            return "error";
        }
    }

    @PostMapping("/user{user_id}/boards/board{board_id}/edit")
    public String updateBoard(@PathVariable(name = "user_id")Long userID,
                              @PathVariable(name = "board_id") Long boardID,
                              @RequestParam(name = "title") String title,
                              Model model) {
        try {
            boardsService.updateBoardTitle(boardID, title);
        } catch (Exception e) {
            model.addAttribute("title", "Неверные данные");
            return "error";
        }

        return "redirect:/user" + userID + "/boards";
    }

}
