package ru.blm.todoapp.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import ru.blm.todoapp.controllers.responses.BoardResponse;
import ru.blm.todoapp.controllers.responses.CardResponse;
import ru.blm.todoapp.database.entities.Card;
import ru.blm.todoapp.database.entities.CardStatus;
import ru.blm.todoapp.database.exceptions.BoardNotFoundException;
import ru.blm.todoapp.services.implementations.BoardsService;
import ru.blm.todoapp.services.implementations.CardsServiceImpl;

import java.util.Date;
import java.util.List;

@Controller
public class CardsController {

    @Autowired
    private BoardsService boardsService;
    @Autowired
    private CardsServiceImpl cardsService;

    @GetMapping("/user{user_id}/boards/board{board_id}/cards")
    public String getCards(@PathVariable(name = "user_id") Long userID,
                           @PathVariable(name = "board_id") Long boardID,
                           Model model) {
        try {
            List<CardResponse> responses = boardsService.getCardResponses(boardID, userID);
            BoardResponse response = boardsService.getBoardResponse(boardID);

            model.addAttribute("board", response);
            model.addAttribute("cards", responses);
        } catch (BoardNotFoundException e) {
            return "error";
        }
        return "cards";
    }

    @GetMapping("/user{user_id}/boards/board{board_id}/cards/add")
    public String addCardPage(@PathVariable(name = "user_id") Long userID,
                              @PathVariable(name = "board_id") Long boardID,
                              Model model) {
        try {
            BoardResponse response = boardsService.getBoardResponse(boardID);

            model.addAttribute("board", response);

            return "addCard";
        } catch (Exception e) {
            return "error";
        }
    }

    @PostMapping("/user{user_id}/boards/board{board_id}/cards/add")
    public String addCard(@PathVariable(name = "user_id") Long userID,
                          @PathVariable(name = "board_id") Long boardID,
                          @RequestParam(name = "title") String title,
                          @RequestParam(name = "description") String description,
                          @RequestParam(name = "end_date")
                              @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) Date endDate,
                          Model model) {
        try {
            Card card = new Card();
            card.setCompleted(false);
            card.setStatus(CardStatus.IN_PROCESS);
            card.setName(title);
            card.setDescription(description);
            card.setEndDate(endDate);
            cardsService.saveCard(card, boardID);
            return "redirect:/user" + userID + "/boards/board" + boardID + "/cards";
        } catch (Exception e) {
            return "error";
        }
    }

    @GetMapping("/user{user_id}/boards/board{board_id}/cards/card{card_id}/delete")
    public String deleteCard(@PathVariable(name = "user_id") Long userID,
                             @PathVariable(name = "board_id") Long boardID,
                             @PathVariable(name = "card_id") Long cardID,
                             Model model) {
        try {
            cardsService.deleteCardByID(cardID);

            return "redirect:/user" + userID + "/boards/board" + boardID + "/cards";
        } catch (Exception e) {
            return "error";
        }
    }

    @GetMapping("/user{user_id}/boards/board{board_id}/cards/card{card_id}/update")
    public String getUpdatePage(@PathVariable(name = "user_id") Long userID,
                                @PathVariable(name = "board_id") Long boardID,
                                @PathVariable(name = "card_id") Long cardID,
                                Model model) {
        try {
            CardResponse response = cardsService.getCardResponseByID(cardID, boardID, userID);

            model.addAttribute("card", response);

            return "editCard";
        } catch (Exception e) {
            return "error";
        }
    }

    @PostMapping("/user{user_id}/boards/board{board_id}/cards/card{card_id}/update")
    public String updateCard(@PathVariable(name = "user_id") Long userID,
                             @PathVariable(name = "board_id") Long boardID,
                             @PathVariable(name = "card_id") Long cardID,
                             @RequestParam(name = "title") String title,
                             @RequestParam(name = "description") String description,
                             @RequestParam(name = "completed", defaultValue = "false") Boolean completed,
                             @DateTimeFormat(iso = DateTimeFormat.ISO.DATE)
                                 @RequestParam(name = "end_date") Date endDate,
                             Model model) {
        try {
            cardsService.updateCard(cardID, title, description, endDate, completed);

            return "redirect:/user" + userID + "/boards/board" + boardID + "/cards";
        } catch (Exception e) {
            return "error";
        }
    }
}
