package ru.blm.todoapp.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import ru.blm.todoapp.database.entities.User;
import ru.blm.todoapp.services.exceptions.RegistrationException;
import ru.blm.todoapp.services.implementations.UserServiceImpl;

@Controller
@RequestMapping("/log-in")
public class LogInController {

    @Autowired
    UserServiceImpl userService;

    @GetMapping
    public String getLogInPage() {
        return "log-in";
    }

    @PostMapping
    public String logInUser(@RequestParam(name = "email") String email,
                            @RequestParam(name = "password") String password,
                            Model model) {
        try {
            User u = userService.register(email, password);
            System.out.println(u);
            return "redirect:/user" + u.getId() + "/boards";
        } catch (RegistrationException e) {
            model.addAttribute("title", "Ошибка регистрации");
            return "error";
        }

    }

}
