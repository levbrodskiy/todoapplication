-readme приложения TODO app-
 --Автор проекта Бродский Лев--
Проект состоит из следующих подсистем:
*configs
*controllers
*db(dataBase)
*exceptions
*services
*utils
*resources

На базе сервиса используется фреймворк - Spring boot
Пример фреймворка:

@GetMapping("/user{user_id}/boards")
    public String getBoards(@PathVariable(name = "user_id")Long userID,
                            Model model) {
        List<BoardResponse> responses = new ArrayList<>();
        try {
            responses = userService.getBoardResponsesByUserID(userID);

        } catch (UserNotFoundException e) {
            return "error";
        }


Библиотека формирования html,css и js структуры - bootstrap
Пример библиотеки:

<div id="container">
  <h1 align="center" class=""><p>При создании доски, не пострадало ни одно дерево</p></h1>
  <form action="" method="post" class="needs-validation" novalidate>
    <div class="form-group ">
      <label for="boardName" >Название вашей доски</label>
      <input name="title" type="text" class="form-control" id="boardName" required>

    </div>
    <div class="form-group ">
      <label>Текст для вашей доски</label>
     <textarea name="description" class="form-control" id="exampleFormControlTextarea1" rows="3" required></textarea>
    </div>
    <div class="d-flex justify-content-center m-3">
        <button type="submit" class="btn btn-primary btn-lg">Создать</button>
    </div>
  </form>
</div>

Для прочтения документации проекта, необходимо посетить вики репозитория 


