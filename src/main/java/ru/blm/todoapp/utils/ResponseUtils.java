package ru.blm.todoapp.utils;

import lombok.NonNull;
import ru.blm.todoapp.controllers.responses.BoardResponse;
import ru.blm.todoapp.controllers.responses.CardResponse;
import ru.blm.todoapp.controllers.responses.UserResponse;
import ru.blm.todoapp.database.entities.Board;
import ru.blm.todoapp.database.entities.Card;
import ru.blm.todoapp.database.entities.User;

import java.util.ArrayList;
import java.util.List;

public class ResponseUtils {
    public static UserResponse getUserResponse(@NonNull User user) {
        UserResponse response = new UserResponse();
        response.setId(user.getId());
        response.setEmail(user.getEmail());
        return response;
    }

    public static List<CardResponse> getResponsesByCardsAndUserID(List<Card> cards, long userID, long boardID) {
        List<CardResponse> responses = new ArrayList<>();

        for (Card card : cards) {
            responses.add(getCardResponse(card, boardID, userID));
        }

        return responses;
    }

    public static CardResponse getCardResponse(Card card, long boardID, long userID) {
        CardResponse response = new CardResponse();
        response.setDescription(card.getDescription());
        response.setId(card.getId());
        response.setTitle(card.getName());

        response.setEndDate(card.getEndDate());
        response.setComplete(card.getCompleted());

        response.setBoardID(boardID);
        response.setUserID(userID);

        response.setStatus(card.getStatus().toString());

        return response;
    }

    public static List<BoardResponse> getBoardResponses(List<Board> boards, long userID) {
        List<BoardResponse> responses = new ArrayList<>();

        for (Board board : boards) {
            BoardResponse response = new BoardResponse();
            response.setBoardID(board.getId());
            response.setTitle(board.getName());
            response.setUserID(userID);
            responses.add(response);
        }

        return responses;
    }

    public static BoardResponse getBoardResponse(Board board) {
        BoardResponse response = new BoardResponse();
        response.setBoardID(board.getId());
        response.setTitle(board.getName());
        response.setUserID(board.getUser().getId());
        return response;
    }
}
