package ru.blm.todoapp.services.implementations;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.blm.todoapp.controllers.responses.BoardResponse;
import ru.blm.todoapp.controllers.responses.UserResponse;
import ru.blm.todoapp.database.entities.Board;
import ru.blm.todoapp.database.entities.User;
import ru.blm.todoapp.database.exceptions.CRUDException;
import ru.blm.todoapp.database.exceptions.LimitOfBoardsException;
import ru.blm.todoapp.database.exceptions.UserNotFoundException;
import ru.blm.todoapp.database.repositories.UserRepository;
import ru.blm.todoapp.services.UserService;
import ru.blm.todoapp.services.exceptions.AuthorizationException;
import ru.blm.todoapp.services.exceptions.RegistrationException;
import ru.blm.todoapp.utils.ResponseUtils;

import java.util.List;

@Service
public class UserServiceImpl implements UserService {
    @Autowired
    private UserRepository userRepository;


    public User register(String email, String password) throws RegistrationException {
        User u = userRepository.findByEmail(email);

        if (u != null) {
            throw new RegistrationException();
        }

        User user = new User();
        user.setEmail(email);
        user.setPassword(password);

        userRepository.saveAndFlush(user);
        return user;
    }

    public User authorize(String email, String password) throws AuthorizationException {
        User user = userRepository.findByEmail(email);

        if (user == null) {
            throw new AuthorizationException();
        }

        if (!user.getPassword().equals(password)) {
            throw new AuthorizationException();
        }

        return user;
    }

    public UserResponse getUserResponse(long userID) throws UserNotFoundException {
        User user = userRepository.getOne(userID);

        return ResponseUtils.getUserResponse(user);
    }

    public void addBoard(Board board, long userID) throws CRUDException, UserNotFoundException, LimitOfBoardsException {
        User user = userRepository.getOne(userID);

        if (user == null) {
            throw new UserNotFoundException();
        }

        if (user.getBoards().size() >= 5) {
            throw new LimitOfBoardsException();
        }

        user.addBoard(board);

        userRepository.saveAndFlush(user);
    }

    public List<BoardResponse> getBoardResponsesByUserID(long userID) throws UserNotFoundException {
        User user = userRepository.getOne(userID);

        if (user == null) {
            throw new UserNotFoundException();
        }

        return ResponseUtils.getBoardResponses(user.getBoards(), userID);
    }
}
