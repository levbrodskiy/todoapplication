package ru.blm.todoapp.services.implementations;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.blm.todoapp.controllers.responses.BoardResponse;
import ru.blm.todoapp.controllers.responses.CardResponse;
import ru.blm.todoapp.database.entities.Board;
import ru.blm.todoapp.database.entities.Card;
import ru.blm.todoapp.database.entities.User;
import ru.blm.todoapp.database.exceptions.BoardNotFoundException;
import ru.blm.todoapp.database.exceptions.UserNotFoundException;
import ru.blm.todoapp.database.repositories.BoardRepository;
import ru.blm.todoapp.database.repositories.CardRepository;
import ru.blm.todoapp.database.repositories.UserRepository;
import ru.blm.todoapp.utils.ResponseUtils;

import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class BoardsService {

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private BoardRepository boardRepository;

    @Autowired
    private CardRepository cardRepository;

    @Transactional
    public void deleteBoard(long id){

        boardRepository.deleteById(id);

    }

    @Transactional
    public void updateBoardTitle(long boardID, String title) {
        Board board = boardRepository.getOne(boardID);
        board.setName(title);
        boardRepository.saveAndFlush(board);
    }

    @Transactional
    public Board getBoard(Long id) {
        return boardRepository.getOne(id);
    }

    @Transactional
    public BoardResponse getBoardResponse(Long id) {
        return ResponseUtils.getBoardResponse(boardRepository.getOne(id));
    }

    @Transactional
    public List<CardResponse> getCardResponses(long boardID, long userID) throws BoardNotFoundException {
        Board board = boardRepository.getOne(boardID);

        if (board == null) {
            throw new BoardNotFoundException();
        }

        if (board.getCards() == null || board.getCards().size() == 0) {
            return new ArrayList<>();
        }

        List<CardResponse> responses =
                ResponseUtils.getResponsesByCardsAndUserID(board.getCards(), userID, boardID);

        return responses;
    }

    @Transactional
    public void addBoardToUser(Board board, long userID) throws Exception {
        try {
            User user = userRepository.getOne(userID);

            if (user == null) {
                throw new UserNotFoundException();
            }

            user.addBoard(board);
            boardRepository.saveAndFlush(board);
            userRepository.saveAndFlush(user);
        } catch (Exception e) {
            throw new Exception();
        }
    }
}
