package ru.blm.todoapp.services.implementations;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.blm.todoapp.controllers.responses.CardResponse;
import ru.blm.todoapp.database.entities.Board;
import ru.blm.todoapp.database.entities.Card;
import ru.blm.todoapp.database.entities.CardStatus;
import ru.blm.todoapp.database.repositories.BoardRepository;
import ru.blm.todoapp.database.repositories.CardRepository;
import ru.blm.todoapp.utils.ResponseUtils;
import javax.transaction.Transactional;
import java.util.Date;

@Service
public class CardsServiceImpl {
    @Autowired
    private CardRepository cardRepository;
    @Autowired
    private BoardRepository boardRepository;

    public Card saveCard(Card card, Long boardID) {
        Board board = boardRepository.getOne(boardID);
        card.setBoard(board);
        cardRepository.saveAndFlush(card);
        return card;
    }

    @Transactional
    public void deleteCardByID(long id) {
        cardRepository.deleteById(id);
    }

    @Transactional
    public void updateCard(Long id, String title, String description, Date endDate, Boolean completed) {
        Card card = cardRepository.getOne(id);

        card.setCompleted(completed);
        if (completed) {
            card.setStatus(CardStatus.COMPLETED);
        } else {
            if (endDate.before(new Date())) {
                card.setStatus(CardStatus.NOT_COMPLETED);
            } else {
                card.setStatus(CardStatus.IN_PROCESS);
            }
        }
        card.setName(title);
        card.setDescription(description);
        card.setEndDate(endDate);

        cardRepository.saveAndFlush(card);
    }

    @Transactional
    public CardResponse getCardResponseByID(long cardID, long boardID, long userID){
        Card card = cardRepository.getOne(cardID);
        if (card.getEndDate() == null) {
            card.setStatus(CardStatus.IN_PROCESS);
        } else {
            if (card.getEndDate().before(new Date())) {
                card.setStatus(CardStatus.NOT_COMPLETED);
            } else {
                card.setStatus(CardStatus.IN_PROCESS);
            }
        }
        if (card.getCompleted()) {
            card.setStatus(CardStatus.COMPLETED);
        }
        cardRepository.saveAndFlush(card);
        return ResponseUtils.getCardResponse(card, boardID, userID);
    }
}
