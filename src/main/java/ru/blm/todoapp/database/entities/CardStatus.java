package ru.blm.todoapp.database.entities;

public enum  CardStatus {
    IN_PROCESS, COMPLETED, NOT_COMPLETED
}
