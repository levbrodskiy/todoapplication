package ru.blm.todoapp.database.entities;

import lombok.Data;
import org.hibernate.annotations.ColumnDefault;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import java.util.Date;

@Data
@Entity
@Table(name = "cards")
public class Card {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;

    @Column(name = "name")
    private String name;

    @Column(name = "description")
    private String description;

    @Temporal(value = TemporalType.DATE)
    @Column(name = "end_date")
    private Date endDate;

    @Column(name = "completed")
    @ColumnDefault(value = "false")
    private Boolean completed;

    @Enumerated(EnumType.STRING)
    @Column(name = "status")
    private CardStatus status;

    @ManyToOne(fetch = FetchType.EAGER, optional = false)
    @JoinColumn(name = "board_id")
    private Board board;
}
