package ru.blm.todoapp.database.entities;

import lombok.Data;

import javax.persistence.*;
import java.util.List;

@Data
@Entity
@Table(name = "boards")
public class Board {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;

    @Column(name = "name")
    private String name;

    @ManyToOne(optional = false, fetch = FetchType.EAGER)
    @JoinColumn(name = "user_id", nullable = false)
    private User user;

    @OneToMany(mappedBy = "board", orphanRemoval = true, fetch = FetchType.EAGER)
    private List<Card> cards;

    public Board() {

    }

    public Board(String name) {
        this.name = name;
    }

    public boolean addCard(Card card) {
        card.setBoard(this);
        cards.add(card);
        return true;
    }
}
