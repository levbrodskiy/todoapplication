package ru.blm.todoapp.database.entities;

import lombok.Data;

import javax.persistence.*;
import java.io.Serializable;
import java.util.List;

@Data
@Entity
@Table(name = "users")
public class User implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;

    @Column(name = "email")
    private String email;

    @Column(name = "password")
    private String password;

    @OneToMany(mappedBy = "user", fetch = FetchType.EAGER, orphanRemoval = true)
    private List<Board> boards;

    public void addBoard(Board board) {
        board.setUser(this);
        boards.add(board);
    }

    public boolean deleteBoard(Board board) {
        board.setUser(null);
        return this.boards.remove(board);
    }

    public Board getBoardByID(long id) {
        return boards.stream().filter(board -> board.getId().equals(id)).findFirst().orElse(null);
    }
}
