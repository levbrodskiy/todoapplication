package ru.blm.todoapp.database.exceptions;

public class LimitOfBoardsException extends Exception {
    public LimitOfBoardsException() {
    }

    public LimitOfBoardsException(String message) {
        super(message);
    }

    public LimitOfBoardsException(String message, Throwable cause) {
        super(message, cause);
    }

    public LimitOfBoardsException(Throwable cause) {
        super(cause);
    }

    public LimitOfBoardsException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
