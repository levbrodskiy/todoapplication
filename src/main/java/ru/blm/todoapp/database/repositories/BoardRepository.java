package ru.blm.todoapp.database.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ru.blm.todoapp.database.entities.Board;

@Repository
public interface BoardRepository extends JpaRepository<Board, Long> {
}
