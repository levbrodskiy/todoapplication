package ru.blm.todoapp.database.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ru.blm.todoapp.database.entities.Card;

@Repository
public interface CardRepository extends JpaRepository<Card, Long> {
}
