package ru.blm.todoapp.utils;

import org.junit.Assert;
import org.junit.Test;
import ru.blm.todoapp.controllers.responses.BoardResponse;
import ru.blm.todoapp.controllers.responses.CardResponse;
import ru.blm.todoapp.controllers.responses.UserResponse;
import ru.blm.todoapp.database.entities.Board;
import ru.blm.todoapp.database.entities.Card;
import ru.blm.todoapp.database.entities.CardStatus;
import ru.blm.todoapp.database.entities.User;

import java.util.Date;

import static org.junit.Assert.*;

public class ResponseUtilsTest {

    @Test
    public void getUserResponse() {
        User user = new User();
        user.setId(5L);
        user.setEmail("email");
        UserResponse response = ResponseUtils.getUserResponse(user);
        Assert.assertEquals(response.getId(), user.getId());
        Assert.assertEquals(response.getEmail(), user.getEmail());
    }

    @Test
    public void getCardResponse() {
        Card card = new Card();
        card.setStatus(CardStatus.IN_PROCESS);
        card.setEndDate(new Date(5));
        card.setName("name");
        card.setDescription("description");
        card.setCompleted(false);
        card.setId(5L);
        CardResponse response = ResponseUtils.getCardResponse(card, 5, 5);
        Assert.assertEquals(card.getStatus().toString(), response.getStatus());
        Assert.assertEquals(card.getId(), response.getId());
        Assert.assertEquals(response.getBoardID(), new Long(5));
        Assert.assertEquals(response.getUserID(), new Long(5));
    }

    @Test
    public void getBoardResponse() {
        Board board = new Board();
        board.setName("n");
        board.setId(5L);
        User user = new User();
        user.setId(5L);
        board.setUser(user);
        BoardResponse response = ResponseUtils.getBoardResponse(board);
        Assert.assertEquals(response.getBoardID(), board.getId());
        Assert.assertEquals(response.getTitle(), board.getName());
        Assert.assertEquals(response.getUserID(), board.getUser().getId());
    }
}